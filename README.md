# tdc_crm_connect


**WP-CONFIG:**

* define('ALLOW_UNFILTERED_UPLOADS', true); 
* define('JWT_AUTH_SECRET_KEY', 'key_here'); 
* define('JWT_AUTH_CORS_ENABLE', true);


**JWT & ACF PLUGIN**
* https://api.wordpress.org/secret-key/1.1/salt/
* https://pl.wordpress.org/plugins/jwt-authentication-for-wp-rest-api/
* https://www.advancedcustomfields.com/pro/

